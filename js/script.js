function initialize() {
    var mapProp = {
        center: new google.maps.LatLng(43.352914, 17.793048),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    };

    var map = new google.maps.Map(document.getElementById("contact-map"), mapProp);

}

google.maps.event.addDomListener(window, 'load', initialize);



// Set active class
function setActive(position, elemPosition, elems){

    if(position < elemPosition.about){
        elems[0].classList.add('active');
    }
    else if(position >= elemPosition.about && position < elemPosition.help){
        elems[1].classList.add('active');
    }
    else if(position >=  elemPosition.help && position < elemPosition.contact){
        elems[2].classList.add('active');
    }
    else if( position >= elemPosition.contact){
        elems[3].classList.add('active');
    }
}
//Remove active class
function removeActive(items) {
    var len = items.length,
        i;
    for (i = 0; i < len; i++) {
        items[i].classList.remove ('active');
    }
}

/*************
 ***Nav-bar***
 *************/

//Store nav-bar elements
var navbar = document.getElementsByClassName('nav-bar')[0];
    navLinks = navbar.getElementsByTagName('a');
    
var navbarPosition = {
    applogo: document.getElementById('applogo').offsetTop -85,
    about: document.getElementById('about').offsetTop -150,
    help: document.getElementById('help').offsetTop -80,
    contact: document.getElementById('contact').offsetTop -90

};   

//Bind nav-bar section events
Object.keys(navLinks).forEach(function (key) {
    var elem = navLinks[key];

    elem.onclick = function (e) {
        var hash = e.target.hash,
            position;
        
        e.preventDefault();
        e.stopPropagation();
        
        removeActive(navLinks); 
        this.classList.add('active'); 
        
        position = navbarPosition[hash.substring(1,hash.length)];
        window.scrollTo(0, position);
    };

});

//Update navigation class on scroll position

var scrollPos = window.scrollY,
    timeout;

setActive(scrollPos, navbarPosition, navLinks);

window.addEventListener('scroll',function(e){
    clearTimeout(timeout);
    
    timeout=setTimeout(function(){
        scrollPos = window.scrollY;
        
        removeActive(navLinks);
        setActive(scrollPos,navbarPosition,navLinks);
    }, 100)
})	
/*************
 **Platforms**
 *************/    
    
  //Store platforms elements

var platformsButt = document.getElementsByClassName('platforms-button')
var platformsBox = document.getElementsByClassName('platforms-change-box')
var platformsImages = document.getElementsByClassName('platforms-image')
var platfomsImageBox = document.getElementsByClassName('platforms-box')

  //Bind click events on buttons
Object.keys(platformsButt).forEach(function (key) {
    var elem = platformsButt[key];

    elem.onclick = function (e) {
        
        e.preventDefault();
        e.stopPropagation();
        
        removeActive(platformsButt); 
        this.classList.add('active'); 
        removeActive(platformsBox);

        if(platformsButt[0].classList.contains('active')){
            platformsBox[0].classList.add('active'); 
        }else{
            platformsBox[1].classList.add('active'); 
        }
        
    };

});

 //Bind click events on platforms images
 Object.keys(platformsImages).forEach(function (key) {
    var elem = platformsImages[key];

    elem.onclick = function (e) {
        
        e.preventDefault();
        e.stopPropagation();
        
        removeActive(platformsImages); 
        this.classList.add('active');

        removeActive(platfomsImageBox);

        if((platformsImages[0].classList.contains('active')) || ((platformsImages[5].classList.contains('active')))){
           platfomsImageBox[0].classList.add('active'); 
        }else if((platformsImages[1].classList.contains('active')) || ((platformsImages[6].classList.contains('active')))){
            platfomsImageBox[1].classList.add('active'); 
        }else if((platformsImages[2].classList.contains('active')) || ((platformsImages[7].classList.contains('active')))){
           platfomsImageBox[2].classList.add('active'); 
        }else if((platformsImages[3].classList.contains('active')) || ((platformsImages[8].classList.contains('active')))){
           platfomsImageBox[3].classList.add('active');
        }else if(platformsImages[4].classList.contains('active')){
            platfomsImageBox[4].classList.add('active'); 
        }    
    };

});


/*************
 **About-app**
 *************/    
    
  //Store platforms elements

var aboutappButt = document.getElementsByClassName('about-app-butt')
var aboutappBox =  document.getElementsByClassName ('about-app-box')

  //Bind click events
Object.keys(aboutappButt).forEach(function (key) {
    var elem = aboutappButt[key];

    elem.onclick = function (e) {
        
        e.preventDefault();
        e.stopPropagation();
        
        removeActive(aboutappButt); 
        this.classList.add('active'); 

        removeActive(aboutappBox);

        if(aboutappButt[0].classList.contains('active')){
            aboutappBox[0].classList.add('active'); 
        }else if(aboutappButt[1].classList.contains('active')){
            aboutappBox[1].classList.add('active'); 
        }else{
            aboutappBox[2].classList.add('active'); 
        }
    };

});

/*************
 ***About-Us***
 *************/    
    
  //Store platforms elements

var aboutusButt = document.getElementsByClassName('about-us-butt')
var aboutusBox = document.getElementsByClassName('about-us-text-box')

  //Bind click events
Object.keys(aboutusButt).forEach(function (key) {
    var elem = aboutusButt[key];

    elem.onclick = function (e) {
        
        e.preventDefault();
        e.stopPropagation();
        
        removeActive(aboutusButt); 
        this.classList.add('active');

         removeActive(aboutusBox);

        if(aboutusButt[0].classList.contains('active')){
           aboutusBox[0].classList.add('active'); 
        }else if(aboutusButt[1].classList.contains('active')){
            aboutusBox[1].classList.add('active'); 
        }else{
            aboutusBox[2].classList.add('active'); 
        } 
    };

});

/**********
 ***Form***
 **********/

// Iskreno mogao prepisati, ali nije bas da je sve jasno
